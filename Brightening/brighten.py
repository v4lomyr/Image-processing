import cv2
import numpy

def brightenImage(imagePath, imageTarget, addedIntensity):
    imageFile = cv2.imread(imagePath)

    intensityMatrix = numpy.ones(imageFile.shape, dtype="uint8") * addedIntensity

    newImage = cv2.add(imageFile, intensityMatrix)

    cv2.imwrite(imageTarget, newImage)

brightenImage("Test_image.png", "brighten.png", 60)