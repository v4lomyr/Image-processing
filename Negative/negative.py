import cv2
import numpy

def gambarNegative(imgPath, targetPath) :
    imageFile = cv2.imread(imgPath)

    negativeImage = negative(imageFile)

    cv2.imwrite(targetPath, negativeImage)

def negative(image) :
    height = image.shape[0]
    width = image.shape[1]
    channels = image.shape[2]

    size = (height, width, channels)

    new_image = numpy.zeros(size, numpy.uint8)
    new_image.fill(255)
    new_image = new_image - image

    return new_image

gambarNegative("Test_Image.png", "negative.png")
