import cv2
import numpy

def gambarBiner(img_path, target_path, threshold):
    image_file = cv2.imread(img_path)                     # --mengambil gambar untuk di proses
    image = cv2.cvtColor(image_file, cv2.COLOR_RGB2GRAY)  # --mengubah gambar RGB menjadi grayscale
    image = numpy.array(image)                            # --membuat array yang berisi value dari pixel pixel gambar grayscale
    image = arrayBiner(image, threshold)                  # --memanggil method untuk membuat array gambar biner berdasarkan gambar grayscale
    cv2.imwrite(target_path, image)                       # --membuat file gambar baru yang berisi gambar biner

def arrayBiner(numpy_array, threshold):
    for i in range(len(numpy_array)):                    # method ini mengiterasi setiap pixel dari gambar grayscale
        for j in range(len(numpy_array[0])):             # menjadi 0 atau 255 dengan ketentuan apabila nilai pixel
            if numpy_array[i][j] > threshold:            # kurang dari threshold yang di beri maka akan bernilai 0
                numpy_array[i][j] = 255                  # dan apabila >= nilai threshold maka akan bernilai 255
            else:
                numpy_array[i][j] = 0
    return numpy_array

gambarBiner('Test_Image.png', 'binary.png', 128)
