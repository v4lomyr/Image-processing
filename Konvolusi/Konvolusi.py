import cv2
import numpy as np

def kernel_rotate(kernel):
    rotated_kernel = kernel.copy()

    for i in range(kernel.shape[0]):
        for j in range(kernel.shape[1]):
            rotated_kernel[i][j] = kernel[kernel.shape[0]-i-1][kernel.shape[1]-j-1]

    return kernel

def convolution(image, kernel):
    kernel = kernel_rotate(kernel)

    image_h = image.shape[0]
    image_w = image.shape[1]

    kernel_h = kernel.shape[0]
    kernel_w = kernel.shape[1]

    height = kernel_h//2 #1,1
    width = kernel_w//2

    image_conv = np.zeros(image.shape)

    for i in range(height, image_h-height):
        for j in range(width, image_w-width):
            sum = 0

            for m in range(kernel_h):
                for n in range(kernel_w):
                    sum = sum + kernel[m][n] * image[i-height+m][j-width+n]

            image_conv[i][j] = sum

    return image_conv

image = cv2.imread("../Test_Image.png")
kernel = [[]]

print("""menu : 
          1. sharpening
          2. blur
          3. edge detection
          
silahkan input nomor : """)
mode = input()
if mode == '1':
    kernel = [[-1 / 2, -1, -1 / 2], [-1, 7, -1], [-1 / 2, -1, -1 / 2]]
    cv2.imwrite("Sharpening_Konvolusi.png", convolution(image, np.array(kernel)))
elif mode == '2':
    kernel = [[1 / 16, 2 / 16, 1 / 16], [2 / 16, 4 / 16, 2 / 16], [1 / 16, 2 / 16, 1 / 16]]
    cv2.imwrite("Blur_Konvolusi.png", convolution(image, np.array(kernel)))
elif mode == '3':
    kernel = [[-1, 0, 1], [-1, 0, 1], [-1, 0, 1]]
    cv2.imwrite("Edge Konvolusi.png", convolution(image, np.array(kernel)))

